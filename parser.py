from __future__ import annotations
import csv
from pathlib import Path
import xml.etree.ElementTree as ET
from pprint import pprint


def main():
    for server_dir in Path("data").iterdir():
        print(server_dir)
        server_data = parser(
            server_dir.joinpath(
                "tsr", "hardware", "sysinfo", "inventory", "sysinfo_DCIM_View.xml"
            )
        )
        write_csv(server_data=server_data)


def write_csv(server_data: dict) -> None:
    root_dir = Path("CSVs")
    for key, value in server_data.items():
        csv_file = root_dir.joinpath(f"{key}.csv")
        with csv_file.open(mode="a") as fout:
            if type(value) is list:
                for item in value:
                    item["service_tag"] = server_data["system"]["service_tag"]
                    fieldnames = item.keys()
                    writer = csv.DictWriter(fout, fieldnames=fieldnames)

                    if fout.tell() == 0:
                        writer.writeheader()

                    writer.writerow(item)
            else:
                fieldnames = value.keys()
                writer = csv.DictWriter(fout, fieldnames=fieldnames)

                if fout.tell() == 0:
                    writer.writeheader()

                writer.writerow(value)


def parser(xml_path: Path) -> dict:
    output = {}

    tree = ET.parse(xml_path)
    root = tree.getroot()
    output["system"] = get_system_info(root)
    output["processors"] = get_processors(root)
    output["memory"] = get_memory_modules(root)
    output["pci_devices"] = get_pci_devices(root)
    output["power_supplies"] = get_power_supplies(root)
    output["controllers"] = get_controllers(root)
    output["storage_enclosures"] = get_storage_enclosures(root)
    output["physical_disks"] = get_physical_disks(root)
    output["virtual_disks"] = get_virtual_disks(root)
    output["ethernet"] = get_ethernet_devices(root)

    return output
    # get_some_tag(root, "DCIM_PCIDeviceView")


def get_system_info(xml_obj) -> dict:
    for instance in xml_obj.findall(".//INSTANCE[@CLASSNAME='DCIM_SystemView']"):
        manufacture = instance.find(".//PROPERTY[@NAME='Manufacturer']")[0].text
        model = instance.find(".//PROPERTY[@NAME='Model']")[0].text
        service_tag = instance.find(".//PROPERTY[@NAME='ServiceTag']")[0].text
        serial = instance.find(".//PROPERTY[@NAME='BoardSerialNumber']")[0].text
        bios = instance.find(".//PROPERTY[@NAME='BIOSVersionString']")[0].text
        cpld = instance.find(".//PROPERTY[@NAME='CPLDVersion']")[0].text
        lc = instance.find(".//PROPERTY[@NAME='LifecycleControllerVersion']")[0].text
        cpu_sockets_used = instance.find(".//PROPERTY[@NAME='PopulatedCPUSockets']")[
            0
        ].text
        cpu_sockets_max = instance.find(".//PROPERTY[@NAME='MaxCPUSockets']")[0].text
        dimm_slots_used = instance.find(".//PROPERTY[@NAME='PopulatedDIMMSlots']")[
            0
        ].text
        dimm_slots_max = instance.find(".//PROPERTY[@NAME='MaxDIMMSlots']")[0].text
        sys_mem = instance.find(".//PROPERTY[@NAME='SysMemTotalSize']")[0].text
        chassis_height = instance.find(".//PROPERTY[@NAME='ChassisSystemHeight']")[
            0
        ].text

        sys_mem = sizeof_fmt_base2(int(sys_mem.strip(" MB")) * 1024**2)

        system = {
            "manufacturer": manufacture,
            "model": model,
            "service_tag": service_tag,
            "serial": serial,
            "BIOS": bios,
            "CPLD": cpld,
            "iDRAC": lc,
            "CPU_sockets_used": cpu_sockets_used,
            "CPU_sockets_max": cpu_sockets_max,
            "DIMM_slots_used": dimm_slots_used,
            "DIMM_slots_max": dimm_slots_max,
            "total_system_memory": sys_mem,
            "chassis_height": chassis_height,
        }

    return system


def get_processors(xml_obj) -> list:
    processors = []
    for instance in xml_obj.findall(".//INSTANCE[@CLASSNAME='DCIM_CPUView']"):
        fqdd = instance.find(".//PROPERTY[@NAME='FQDD']")[0].text
        model = instance.find(".//PROPERTY[@NAME='Model']")[0].text
        clock_cur = instance.find(".//PROPERTY[@NAME='CurrentClockSpeed']")[0].text
        clock_max = instance.find(".//PROPERTY[@NAME='MaxClockSpeed']")[0].text
        cores = instance.find(".//PROPERTY[@NAME='NumberOfProcessorCores']")[0].text
        threads = instance.find(".//PROPERTY[@NAME='NumberOfEnabledThreads']")[0].text
        l1_cache = instance.find(".//PROPERTY[@NAME='Cache1Size']")[0].text
        l2_cache = instance.find(".//PROPERTY[@NAME='Cache2Size']")[0].text
        l3_cache = instance.find(".//PROPERTY[@NAME='Cache3Size']")[0].text
        cpu = {
            "index": fqdd,
            "model": model,
            "clock": f"{clock_cur} (max {clock_max})",
            "cores": cores,
            "threads": threads,
            "L1": l1_cache,
            "L2": l2_cache,
            "L3": l3_cache,
        }
        processors.append(cpu)

    return processors


def get_memory_modules(xml_obj) -> list:
    memory_modules = []
    for instance in xml_obj.findall(".//INSTANCE[@CLASSNAME='DCIM_MemoryView']"):
        index = instance.find(".//PROPERTY[@NAME='FQDD']")[0].text
        vendor = instance.find(".//PROPERTY[@NAME='Manufacturer']")[0].text
        model = instance.find(".//PROPERTY[@NAME='PartNumber']")[0].text
        size = instance.find(".//PROPERTY[@NAME='Size']")[0].text
        clock_cur = instance.find(".//PROPERTY[@NAME='CurrentOperatingSpeed']")[0].text
        clock_max = instance.find(".//PROPERTY[@NAME='Speed']")[0].text
        rank = instance.find(".//PROPERTY[@NAME='Rank']")[0].text
        serial = instance.find(".//PROPERTY[@NAME='SerialNumber']")[0].text
        manufacture = instance.find(".//PROPERTY[@NAME='ManufactureDate']")[0].text

        size = sizeof_fmt_base2(int(size.strip(" MB")) * 1024**2)

        module = {
            "index": index,
            "vendor": vendor,
            "model": model,
            "size": size,
            "clock": f"{clock_cur} (max {clock_max})",
            "rank": rank,
            "serial": serial,
            "manufacture": manufacture,
        }
        memory_modules.append(module)

    return memory_modules


def get_pci_devices(xml_obj) -> list:
    pci_devices = []
    for instance in xml_obj.findall(".//INSTANCE[@CLASSNAME='DCIM_PCIDeviceView']"):
        bus = instance.find(".//PROPERTY[@NAME='BusNumber']")[0].text
        dev = instance.find(".//PROPERTY[@NAME='DeviceNumber']")[0].text
        func = instance.find(".//PROPERTY[@NAME='FunctionNumber']")[0].text
        vendor = instance.find(".//PROPERTY[@NAME='Manufacturer']")[0].text
        desc = instance.find(".//PROPERTY[@NAME='Description']")[0].text
        location = instance.find(".//PROPERTY[@NAME='FQDD']")[0].text
        device = {
            "bus:dev:func": f"{int(bus):03d}:{int(dev):02d}:{int(func):02d}",
            "vendor": vendor,
            "description": desc,
            "location": location,
        }
        pci_devices.append(device)

    return pci_devices


def get_controllers(xml_obj) -> list:
    controllers = []
    for instance in xml_obj.findall(".//INSTANCE[@CLASSNAME='DCIM_ControllerView']"):
        fqdd = instance.find(".//PROPERTY[@NAME='FQDD']")[0].text
        name = instance.find(".//PROPERTY[@NAME='ProductName']")[0].text
        version = instance.find(".//PROPERTY[@NAME='ControllerFirmwareVersion']")[
            0
        ].text
        controller = {"fqdd": fqdd, "name": name, "firmware": version}
        controllers.append(controller)

    return controllers


def get_storage_enclosures(xml_obj) -> list:
    storage_enclosures = []
    for instance in xml_obj.findall(".//INSTANCE[@CLASSNAME='DCIM_EnclosureView']"):
        fqdd = instance.find(".//PROPERTY[@NAME='FQDD']")[0].text
        slots = instance.find(".//PROPERTY[@NAME='SlotCount']")[0].text
        version = instance.find(".//PROPERTY[@NAME='Version']")[0].text
        state = instance.find(".//PROPERTY[@NAME='State']")[0].text
        enclosure = {"fqdd": fqdd, "slots": slots, "state": state, "firmware": version}
        storage_enclosures.append(enclosure)

    return storage_enclosures


def get_power_supplies(xml_obj) -> list:
    power_supplies = []
    for instance in xml_obj.findall(".//INSTANCE[@CLASSNAME='DCIM_PowerSupplyView']"):
        location = instance.find(".//PROPERTY[@NAME='FQDD']")[0].text
        output = instance.find(".//PROPERTY[@NAME='TotalOutputPower']")[0].text
        input_voltage = instance.find(".//PROPERTY[@NAME='InputVoltage']")[0].text
        redundancy = instance.find(".//PROPERTY[@NAME='RedundancyStatus']")[0].text
        pn = instance.find(".//PROPERTY[@NAME='PartNumber']")[0].text
        serial = instance.find(".//PROPERTY[@NAME='SerialNumber']")[0].text
        model = instance.find(".//PROPERTY[@NAME='Model']")[0].text
        firmware = instance.find(".//PROPERTY[@NAME='FirmwareVersion']")[0].text
        psu = {
            "location": location,
            "output": output,
            "input": input_voltage,
            "redundancy": redundancy,
            "part_number": pn,
            "serial": serial,
            "model": model,
            "firmware": firmware,
        }
        power_supplies.append(psu)

    return power_supplies


def get_physical_disks(xml_obj) -> list:
    physical_disks = []
    for instance in xml_obj.findall(".//INSTANCE[@CLASSNAME='DCIM_PhysicalDiskView']"):
        fqdd = instance.find(".//PROPERTY[@NAME='FQDD']")[0].text
        slot = instance.find(".//PROPERTY[@NAME='Slot']")[0].text
        state = instance.find(".//PROPERTY[@NAME='RaidStatus']")[0].text
        status = instance.find(".//PROPERTY[@NAME='PrimaryStatus']")[0].text
        model = instance.find(".//PROPERTY[@NAME='Model']")[0].text
        size = instance.find(".//PROPERTY[@NAME='SizeInBytes']")[0].text
        serial = instance.find(".//PROPERTY[@NAME='SerialNumber']")[0].text
        firmware = instance.find(".//PROPERTY[@NAME='Revision']")[0].text

        size = sizeof_fmt_base10(int(size.strip(" Bytes")))

        disk = {
            "fqdd": fqdd,
            "slot": slot,
            "state": state,
            "status": status,
            "model": model,
            "size": size,
            "serial": serial,
            "firmware": firmware,
        }
        physical_disks.append(disk)

    return physical_disks


def get_virtual_disks(xml_obj) -> list:
    virtual_disks = []
    for instance in xml_obj.findall(".//INSTANCE[@CLASSNAME='DCIM_VirtualDiskView']"):
        fqdd = instance.find(".//PROPERTY[@NAME='FQDD']")[0].text
        name = instance.find(".//PROPERTY[@NAME='Name']")[0].text
        state = instance.find(".//PROPERTY[@NAME='RAIDStatus']")[0].text
        status = instance.find(".//PROPERTY[@NAME='PrimaryStatus']")[0].text
        level = instance.find(".//PROPERTY[@NAME='RAIDTypes']")[0].text
        size = instance.find(".//PROPERTY[@NAME='SizeInBytes']")[0].text

        disks = instance.find(".//PROPERTY.ARRAY[@NAME='PhysicalDiskIDs']/VALUE.ARRAY")
        members = " & ".join([member.text for member in disks])

        size = sizeof_fmt_base10(int(size.strip(" Bytes")))

        disk = {
            "fqdd": fqdd,
            "name": name,
            "state": state,
            "status": status,
            "level": level,
            "size": size,
            "members": members,
        }
        virtual_disks.append(disk)

    return virtual_disks


def get_ethernet_devices(xml_obj) -> list:
    ethernet_devices = []
    for instance in xml_obj.findall(".//INSTANCE[@CLASSNAME='DCIM_NICView']"):
        location = instance.find(".//PROPERTY[@NAME='FQDD']")[0].text
        model = instance.find(".//PROPERTY[@NAME='ProductName']")[0].text
        speed = instance.find(".//PROPERTY[@NAME='LinkSpeed']")[0].text
        duplex = instance.find(".//PROPERTY[@NAME='LinkDuplex']")[0].text
        firmware = instance.find(".//PROPERTY[@NAME='FamilyVersion']")[0].text

        for nic in xml_obj.findall(
            ".//INSTANCE[@CLASSNAME='DCIM_SwitchConnectionView']"
        ):
            if nic.find(".//PROPERTY[@NAME='FQDD']")[0].text == location:
                switch_mac = nic.find(".//PROPERTY[@NAME='SwitchConnectionID']")[0].text
                switch_port = nic.find(".//PROPERTY[@NAME='SwitchPortConnectionID']")[
                    0
                ].text

        nic = {
            "location": location,
            "model": model,
            "speed": speed,
            "duplex": duplex,
            "firmware": firmware,
            "switch_mac": switch_mac if switch_mac else "",
            "switch_port": switch_port if switch_port else "",
        }
        ethernet_devices.append(nic)

    return ethernet_devices


def sizeof_fmt_base2(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f} {unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f} Yi{suffix}"


def sizeof_fmt_base10(num, suffix="B"):
    for unit in ["", "K", "M", "G", "T", "P", "E", "Z"]:
        if abs(num) < 1000.0:
            return f"{num:3.1f} {unit}{suffix}"
        num /= 1000.0
    return f"{num:.1f} Y{suffix}"


def get_some_tag(xml_obj, tag_name) -> list:
    for instance in xml_obj.findall(f".//INSTANCE[@CLASSNAME='{tag_name}']"):
        for elem in instance.iter():
            print(elem.tag, elem.attrib, elem.text)
        # if instance.find(".//PROPERTY[@NAME='Manufacturer']")[0].text == "NVIDIA Corporation":
        #     pass


if __name__ == '__main__':
    main()
