import csv
import json
from pathlib import Path


def get_firmware(dir_path: Path, out_path: Path) -> None:
    file_path = dir_path.joinpath('tsr', 'metadata.json')
    with file_path.open(mode='r') as fin:
        data = json.load(fin)
        with out_path.open(mode='a') as fout:
            fieldnames = data.keys()
            writer = csv.DictWriter(fout, fieldnames=fieldnames)

            if fout.tell() == 0:
                writer.writeheader()

            writer.writerow(data)


out_path = Path('data/firmware.csv')
for server_dir in Path('data').iterdir():
    get_firmware(server_dir, out_path)

