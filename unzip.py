from pathlib import Path
from zipfile import ZipFile


def main():
    zip_dir = Path('zipped_data')
    extracted_dir = Path('data')
    extracted_dir.mkdir(exist_ok=True)
    for item in zip_dir.iterdir():
        server_name = item.name[:-4]
        server_dir = Path.joinpath(extracted_dir, server_name)
        server_dir.mkdir(exist_ok=True)
        extrator(file_path=item, server_dir=server_dir)


def extrator(file_path: Path, server_dir: Path) -> None:
    with ZipFile(file_path, 'r') as zipObj:
        zipObj.extractall(server_dir)

    for current_file in server_dir.iterdir():
        print(file_path, current_file)
        if current_file.suffix == '.zip' and current_file != file_path:
            extrator(current_file, server_dir)


if __name__ == "__main__":
    main()
